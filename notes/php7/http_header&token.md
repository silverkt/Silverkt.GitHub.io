#### PHP http_header & token

##### 1. PHP token

[HTTP请求方法中，如Get/Post方式中的HTTP头 Authorization 怎么添加?](https://segmentfault.com/q/1010000010088192)

[token放在header中好处，HTTP Header详解](http://blog.csdn.net/wabiaozia/article/details/75196787)

[用HttpPost登陆验证时，用户名和密码放在请求头部header中的处理方法，形式为Authorization: username password](http://blog.csdn.net/u013136708/article/details/41210897)

##### 2. PHP header 

[PHP 使用header函数设置HTTP头的示例解析 表头](http://www.cnblogs.com/chengshan/p/6526567.html)

[PHP header函数设置http报文头(设置头部域)](http://www.cnblogs.com/wangyuman26/p/6216717.html)

[PHP中Header使用的HTTP协议及常用方法小结](http://www.jb51.net/article/57098.htm)






