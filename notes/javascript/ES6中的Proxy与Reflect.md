## ES6中的Proxy与Reflect
### Proxy
Proxy,顾名思义为‘代理’，主要为目标对象创建的”代理对象“，用来对目标对象的默认操作进行拦截，值得注意的是，要想使拦截起作用，必须使用“代理对象”来调用这些默认行为。
（1）set(target,prop,val)，对对象属性的赋值操作进行拦截。
（2）get(target,prop),对对象属性的访问进行拦截（“.”访问与表达式访问）。
（3）deleteProperty(target,prop),拦截删除对象属性的操作
（4）ownkeys(target),拦截对象的getOwnPropertyNames操作
（5）has(target,prop)，拦截对对象属性的“in”操作
（6）defineProperty(target,prop,des)拦截对象的Object.defineProperty操作
（7）getOwnPropertyDescriptor(target)拦截对象的Object.getOwnPropertyDescriptor操作
（8）setPrototypeOf(target)拦截对象的Object.setPrototypeOf操作
（9）getPrototypeOf(target)拦截对象的Object.getPrototypeOf操作
（10）apply(target,thisArgs,args),当目标对象是Function时，拦截方法的调用
（11）contruct(target,args),当目标对象是Function时,拦截被new关键字调用创建对象
（12）isExtensiable(target)拦截对象的Object.isExtensiable操作,返回值需要与原对象的isExtensiable的返回值保持一致。
（13）preventExtensions(target)拦截对象的Object.preventExtensions操作，当原对象的isExtensiable值为false时，返回值只能为fasle，否则报错。
### Reflect
Reflect，顾名思义为“映射”，提供了与Proxy一一对应的默认行为，可以作为拦截的基础，当需要对象的默认行为进行扩展时，在Proxy中可以先通过Reflect调用其默认操作，在添加额外的功能，Reflect中的方法改善了Object中的某些方法。
```javascript
//Proxy  主要在执行一些操作之前执行一些拦截，但是执行拦截操作的不是对象本身而是Proxy对象，所以也正如它字面上的意思‘代理’

//Proxy对象的构造函数的形式   Proxy(target,handler)  第一参数为要拦截的目标对象，第二个操作为要拦截对象的属性，和对应的行为
var person = {name:'lixuan',age:21};
var handler = {
    get(target,prop){//对于获取属性的拦截
       return prop==='name'?'hello':target[prop];
   },
   set(target,prop,value){//对于设置属性值的拦截
       target[prop]=prop==='age'?(value>18?value:18):value;
       return true;
   },
   has(target,prop){//对于是否含有某个属性的拦截  in 操作
       if(prop==='age'){
           console.log('age is hidden!');
           return false;
       }
       return prop in target;
   },
   getPrototypeOf(target){//拦截Object.getPrototypeOf操作
      return {x:'1'};
   },
   isExtensible(target){//对于目标对象是否可扩展进行拦截，返回值必须保证和不拦截的时候相等。
       console.log('is Extensible!');
       return true;
   },
   ownKeys(target){//对于Object.getOwnPropertyNames的拦截
       return ['hello','world'];
   },
   setPrototypeOf(target,proto){
       throw new Error('Changing the prototype is forbidden');
   }
};
var proxy = new Proxy(person,handler);
var fn = function(){};
var proxy_fn = new Proxy(fn,{
    apply(target,thisArgs,args){
        console.log('this is Proxy');//对于将调用、apply、call时的拦截,目标对象必须是方法
    }
});

console.log(proxy.name);//hello  因为之前对获取属性的方法进行了拦截，所以返回hello
proxy.age = 12;
console.log(proxy.age);//18  在设置属性值的时候，进行了拦截，若设置的小于18，则设为18；
proxy_fn();//this is Proxy  对fn方法的调用进行了拦截。
console.log('age' in proxy);//age is hidden! false 对判断是否属性的操作进行了拦截
console.log(Object.getPrototypeOf(proxy));//{ x: '1' }
console.log(Object.isExtensible(proxy));//is Extensible! true
console.log(Object.getOwnPropertyNames(proxy));//[ 'hello', 'world' ]
//Object.setPrototypeOf(proxy,{}); //报错



//Proxy.revocable()  创建一个可取消的Proxy实例  返回值为一个Proxy实例和一个取消方法  我自己当前的编辑器不支持
// var [proxy1,revoke] = Proxy.revocable({name:'kermit',age:21},handler);
// console.log(proxy1.age);


//Reflect 主要是将Object上的APIyizhi到Reflect上，以后Object也不会这些方法，也添加和改进了一些原来的方法
//最后Reflect上的行为和Proxy是一一对应的，也就是说Proxy对象中存在的方法，Reflect都有，因此在改变在需要在默认行为中添加额外功能时，
//先调用Reflect中的默认行为，再添加额外的功能，它是Proxy拦截的基础。

//属性相关

//获取属性的get方法

var lixuan = {name:'kermit',age:21}

console.log(Reflect.get(lixuan,'name'));//kermit

//设置属性的set方法
Reflect.set(lixuan,'name','lixuan');
console.log(Reflect.get(lixuan,'name'));//lixuan

//删除属性的deleteProperty方法
Reflect.deleteProperty(lixuan,'age');
console.log(lixuan.age);//undefined

//判断是否含有某个属性
console.log(Reflect.has(lixuan,'age'));//false

//获取对象的所有属性
console.log(Reflect.ownKeys(lixuan));//[ 'name' ]

//定义属性
Reflect.defineProperty(lixuan,'sex',{
    value:'M',
    configurable:false
});

console.log(lixuan.sex);//M


//获取属性描述符 

console.log(Reflect.getOwnPropertyDescriptor(lixuan,'sex'));
/**
 * { value: 'M',
  writable: false,
  enumerable: false,
  configurable: false }
 */

//原型相关

//设置对象原型
Reflect.setPrototypeOf(lixuan,person);

//获取对象原型
console.log(Reflect.getPrototypeOf(lixuan));//{ name: 'lixuan', age: 18 }

//方法有关的

//方法调用
function say(){
    console.log(`name:${this.name},age:${this.age}`);
};
Reflect.apply(say,lixuan,[]);//name,lixuan,age:18

//对象相关

//构造方法的调用

function Animal(name){
   this.name = name;
}

console.log(Reflect.construct(Animal,['dog']));//Animal { name: 'dog' }


//组织对象扩展（添加新属性）
Reflect.preventExtensions(lixuan);

//判断对象是否可扩展
console.log(Reflect.isExtensible(lixuan));//false
```
--------------------------------
### 参考链接: 
[ES6中的Proxy与Reflect](https://zhuanlan.zhihu.com/p/24186636)
[ES6 Proxy/Reflect 浅析](https://segmentfault.com/a/1190000008326517)