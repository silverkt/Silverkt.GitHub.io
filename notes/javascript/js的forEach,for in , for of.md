# js的forEach,for in , for of
## forEach遍历数组
```javascript
var myArry =[1,2,3,4];
myArry.desc ='four';
myArry.forEach((value,index,arr)=>{
  console.log(value);
 
});
//1
//2
//3
//4
```
forEach遍历数组，而不是遍历对象哦，而且在遍历的过程中不能被终止,必须每一个值遍历一遍后才能停下来
注意其与jQuery的$.each类似，只不过参数正好是相反的
$.each([], function(index, value, array) { // ... });
$.each遍历数组或者类数组
第1个和第2个参数正好是相反的，这里要注意了，不要记错了。
## for in遍历对象
```javascript
var myArry =[1,2,3,4];
myArry.desc ='four';
 for(var value in myArry){ //循环key
  console.log(value)
}

//"0"
//"1"
//"2"
//"3"
//"desc" 注意这里添加上去的属性也被遍历出来了
```
循环遍历对象的key，是键值对前面的那一个哦 
一般不推荐遍历数组，因为for in遍历后的不能保证顺序，而且原型链上的属性也会被遍历到，因此一般常用来遍历非数组的对象并且使用hasOwnProperty()方法去过滤掉原型链上的属性
## for of遍历对象
```javascript
var myArry =[1,2,3,4];
myArry.desc ='four';
for(var value of myArry){
  console.log(value)
}
//1
//2
//3
//4
```
循环遍历对象的值，是遍历键值对后面的那一个value哦 ，与for in遍历key相反
这是最简洁、最直接的遍历数组元素的语法
这个方法避开了for-in循环的所有缺陷
与forEach()不同的是，它可以正确响应break、continue和return语句

### 参考链接 [**js的forEach,for in , for of**](http://www.cnblogs.com/ruoqiang/p/6217929.html)