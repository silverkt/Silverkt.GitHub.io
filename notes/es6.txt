let 用来声明变量。它的用法类似于 var ,但是所声明的变量,只在 let 命令所在的代码块内有效。

在代码块内,使用let命令声明变量之前,该变量都是不可用的。这在语法上,称为“暂时性死
区”

在 JavaScript 中, undefined 是一个没有设置值的变量
null是一个只有一个值的特殊类型。表示一个空对象引用。
typeof undefined             // undefined
typeof null                  // object
null === undefined           // false
null == undefined            // true


ES5只有全局作用域和函数作用域,没有块级作用域,这带来很多不合理的场景。
第一种场景,内层变量可能会覆盖外层变量。
第二种场景,用来计数的循环变量泄露为全局变量。


--------------------------------------------------------------
===+++=====================================
http://www.jb51.net/article/81663.htm
1.slice（数组）
用法：array.slice(start,end)
解释：该方法是对数组进行部分截取，并返回一个数组副本；参数start是截取的开始数组索引，end参数等于你要取的最后一个字符的位置值加上1（可选）

2.slice（字符串）
用法：string.slice(start,end)
解释：slice方法复制string的一部分来构造一个新的字符串，用法与参数匀和数组的slice方法一样;end参数等于你要取的最后一个字符的位置值加上1

3.splice（数组）
用法：array.splice(start,deleteCount,item...)
解释：splice方法从array中移除一个或多个数组，并用新的item替换它们。参数start是从数组array中移除元素的开始位置。参数deleteCount是要移除的元素的个数。
如果有额外的参数，那么item会插入到被移除元素的位置上。它返回一个包含被移除元素的数组。

4.split（字符串）
用法：string.split(separator,limit)
解释：split方法把这个string分割成片段来创建一个字符串数组。可选参数limit可以限制被分割的片段数量。separator参数可以是一个字符串或一个正则表达式。如果
separator是一个空字符，会返回一个单字符的数组。


===============================================
说的是 join 和 toString 区别
join 是针对数组，功能是连接数组元素为一个字符串，可以指定连接字符。
比如
var a = ['a', 'b', 'c'];
var s = a.join('&');
alert(s); // 得到字符串 "a&b&c"
toString 针对的对象就比较广了，基本上只要是 javascript 的内建对象，都可以用 toString。具体得到什么内容，要看对象是什么。
比如
var a = ['a', 'b', 'c'];
alert(a.toString());  // 得到字符串 "a,b,c"
var o = {a: 1, b:2}
alert(o.toString());  //  得到字符串 "[object Object]"



