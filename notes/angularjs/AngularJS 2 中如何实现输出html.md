### AngularJS 2 中如何实现输出html
#### 使用[innerHtml]
在AngularJS 1.x 中可以使用ng-bind-html来插入一段html代码，相当于struts2 <s:property>标签的escape属性。但是AngularJS 2中取消了ng-bind-html，当项目中确实有需要动态插入html代码的时候我们该怎么做呢？
使用[innerHtml]代替ng-bind-html。
innerHtml属性,用于设置标签内的html，[innerHtml]=”data.title”用于动态将AngularJS 2的变量值赋给innerHtml属性，以实现ng-bind-html的效果

HTML文档
```html
<a href="#" target="_blank" [innerHtml]="data"></a>
```
TS文档
```typescript
export class SearchComponent {
    private data: string = "<b>helloWorld</b>";
}
```
#### 使用 <pre>
html渲染页面时会多个空格或者换行合并成一个空格显示在页面上
可以在渲染页面时使用<pre></pre>标签，把返回的字符串插入这个标签里。
pre 标签可定义预格式化的文本。被包围在 pre 标签中的文本通常会保留空格和换行符。而文本也会呈现为等宽字体

#### 参考链接

[AngularJS 2 中如何实现ng-bind-html](http://blog.csdn.net/lvhongqiang/article/details/53650586)  http://www.lvhongqiang.com/blog425.html

[angular过滤器中返回的字符串怎么换行](https://segmentfault.com/q/1010000006810073)